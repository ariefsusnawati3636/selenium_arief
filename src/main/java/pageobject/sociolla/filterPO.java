package pageobject.sociolla;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class filterPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public filterPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@class='tabs__item tabs__item__header-filter'][1]")
    private WebElement terlarisFilterButton;

    @FindBy(xpath = "//*[@class='tabs__item tabs__item__header-filter'][2]")
    private WebElement reviewFilterButton;

    @FindBy(xpath = "//*[@class='tabs__item tabs__item__header-filter'][3]")
    private WebElement saleFilterButton;

    @FindBy(xpath = "//*[@class= 'tabs__item tabs__item__header-filter tabs__item__header-filter--with-caret up'][1]")
    private WebElement hargaFilterButton;

    @FindBy(xpath = "//*[@class='tabs__item tabs__item__header-filter'][4]")
    private WebElement terbaruFilterButton;

    @FindBy(xpath = "//*[@class= 'tabs__item tabs__item__header-filter tabs__item__header-filter--with-caret up'][2]")
    private WebElement abjadFilterButton;

    @FindBy(xpath = "//*[@class='product product--v3']")
    private WebElement verifyFilter;

    /**
     * filter terlaris
     * @throws InterruptedException
     */
    public void clickOnTerlarisButton() throws InterruptedException {
        selenium.clickOn(terlarisFilterButton);
    }
    /**
     * filter review
     * @throws InterruptedException
     */
    public void clickOnReviewButton() throws InterruptedException {
        selenium.clickOn(reviewFilterButton);
    }
    /**
     * filter sale
     * @throws InterruptedException
     */
    public void clickOnSaleButton() throws InterruptedException {
        selenium.clickOn(saleFilterButton);
    }
    /**
     * filter harga
     * @throws InterruptedException
     */
    public void clickOnHargaButton() throws InterruptedException {
        selenium.clickOn(hargaFilterButton);
    }
    /**
     * filter terbaru
     * @throws InterruptedException
     */
    public void clickOnTerbaruButton() throws InterruptedException {
        selenium.clickOn(terbaruFilterButton);
    }
    /**
     * filter abjad
     * @throws InterruptedException
     */
    public void clickOnAbjadButton() throws InterruptedException {
        selenium.clickOn(abjadFilterButton);
    }
    /**
     * verifikasi setelah filter
     * @throws InterruptedException
     */
    public boolean verifyFilter() {

        if(selenium.isElementAppeared(verifyFilter))
        {
            return true;
        }
        return false;
    }


}
