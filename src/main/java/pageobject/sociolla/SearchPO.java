package pageobject.sociolla;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class SearchPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@id='globalSearch']")
    private WebElement searchFilterEditText;

    /**
     * input search
     * @param inputSearchFilter
     * @throws InterruptedException
     */
    public void inputSearch (String inputSearchFilter){
        selenium.enterText(searchFilterEditText, inputSearchFilter, true);
        selenium.hitEnterKey(searchFilterEditText, false);
    }

    public void enterSearch () {
        selenium.hitEnterKey(searchFilterEditText, false);
    }

    public void hardWait(int seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000);
    }
}
