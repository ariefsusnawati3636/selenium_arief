package pageobject.sociolla;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class AddToCartPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public AddToCartPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }


    @FindBy(xpath = "(//*[@class='product__name'][@target='_blank'])")
    private WebElement shopNowButton;

    @FindBy(xpath = "(//*[@class='btn-add-to-bag-mobile button-add-to-bag rounded'][@type='submit'])")
    private WebElement addToBagButton;

    @FindBy(xpath = "//*[@class='mybag_wrapper']")
    private WebElement myBagText;

    @FindBy(xpath = "//*[@class='button-checkout']")
    private WebElement checkoutButton;

    @FindBy(xpath = "//*[@class='list-step__text']")
    private WebElement verifyBuyNow;

    /**
     * click button shop now
     * @throws InterruptedException
     */
    public void clickShopNowButton() throws InterruptedException {
        selenium.click(shopNowButton);
    }
    /**
     * click button add to bag
     * @throws InterruptedException
     */
    public void clickOnAddToBagButton() throws InterruptedException {
        selenium.clickOn(addToBagButton);
    }
    /**
     * click button shop now
     * @throws InterruptedException
     */
    public void clickMyBagText() throws InterruptedException {
        selenium.click(myBagText);
    }
    /**
     * click button checkout
     * @throws InterruptedException
     */
    public void clickOnCheckoutButton() throws InterruptedException {
        selenium.clickOn(checkoutButton);
    }
    /**
     * Verify chekout pada page pembayaran
     * @throws InterruptedException
     */
    public boolean verifyBuyNow() {
        if(selenium.isElementAppeared(verifyBuyNow))
        {
            return true;
        }
        return false;
    }
}
