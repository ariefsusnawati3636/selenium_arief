package pageobject.matahari;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class AddToCartPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddToCartPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    @FindBy(xpath = "//*[@class='action tocart primary'][1]")
    private WebElement btnBeli;

    @FindBy(xpath = "//*[@id=\"product-options-wrapper\"]/div/div/div/div[1]/div/div[1]")
    private WebElement choseSize;

    @FindBy(xpath = "//*[@id=\"product-options-wrapper\"]/div/div/div/div[3]/div/div[1]")
    private WebElement choseColor;

    @FindBy(id = "product-addtocart-button")
    private WebElement addTocartBtn;

    @FindBy(xpath = "//a[@class='action showcart']")
    private WebElement showCart;

    @FindBy(xpath = "//*[@id=\"product_addtocart_form\"]/div[2]/div")
    private WebElement itemNameCart;


    /**
     * Click button beli
     * @throws InterruptedException throws exception
     */
    public void clickOnBuyButton() throws InterruptedException {
        selenium.clickOn(btnBeli);
    }

    /**
     * Chose Size
     * @throws InterruptedException throws exception
     */
    public void clickOnChoseSize() throws InterruptedException {
        selenium.clickOn(choseSize);
    }

    /**
     * Chose color
     * @throws InterruptedException throws exception
     */
    public void clickOnChoseColor()  throws InterruptedException{
        selenium.clickOn(choseColor);
    }

    /**
     * Click add to cart button
     * @throws InterruptedException throws exception
     */

    public void clickOnAddToCart() throws InterruptedException {
        selenium.clickOn(addTocartBtn);
    }

    /**
     * Click Show cart
     * @throws InterruptedException throws exception
     */
    public void clickOnShowCart() throws InterruptedException {
        selenium.clickOn(showCart);
    }

    /**
     * verify item name on cart
     *
     * @throws InterruptedException throws exception
     */
    public boolean verifyItemNameCart() {
        return selenium.isElementAppeared(itemNameCart);
    }
}




