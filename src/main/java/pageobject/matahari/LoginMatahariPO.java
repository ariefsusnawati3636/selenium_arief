package pageobject.matahari;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class LoginMatahariPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public LoginMatahariPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    @FindBy(xpath = "//a[@class='navbar-link authorization-link']")
    private WebElement linkLogin;

    @FindBy(id = "email")
    private WebElement emailText;

    @FindBy(id = "pass")
    private WebElement passwordText;

    @FindBy(id = "send2")
    private WebElement loginButton;

    @FindBy(xpath = "//div[@id='first-characters']")
    private WebElement linkProfil;

    /**
     * Click Navbar login
     *@throws InterruptedException
     */
    public void clickOnNavbarLogin() throws InterruptedException {
        selenium.clickOn(linkLogin);
    }

    /**
     * Enter email or phone number
     * @param email
     *@throws InterruptedException
     */
    public void enterEmail(String email) throws InterruptedException {
        selenium.enterText(emailText, email, true);
    }

    /**
     * Enter password
     */
    public void enterPassword(String password) throws InterruptedException {
        selenium.enterText(passwordText, password, true);
    }

    /**
     * Enter Login Button
     * @throws InterruptedException
     */
    public void clickOnLoginButton() throws InterruptedException {
        selenium.click(loginButton);
    }

    /**
     * verify Profil name
     * @throws InterruptedException
     */
    public boolean verifyProfilPicture() {
      if  (selenium.isElementAppeared(linkProfil)){
          return true;
      }
      return false;
    }
}

