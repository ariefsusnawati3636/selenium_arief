package pageobject.matahari;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

import java.util.List;

public class FilterBarangPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    public FilterBarangPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }
    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    @FindBy(xpath = "//*[@id=\"narrow-by-list\"]/div[4]/div[1]/div")
    private WebElement filterColor;

    @FindBy(xpath = "//*[@id=\"narrow-by-list\"]/div[4]/div[2]/form/div/div[1]/a/div")
    private WebElement listFilter;
    /**
     * Selector color
     *   @FindBy(xpath = "//*[@id=\"amasty-shopby-product-list\"]/div[2]/ol/li/div/div/div[2]/div/span[1]/span")
     *   private WebElement Color;
     * /

    /**
     * Switch Filter
     * @throws InterruptedException throws exception
     */
    public void clickOnSwitchFilter() throws InterruptedException {
        selenium.clickOn(filterColor);
    }

    /**
     * Click on list filter
     * @throws InterruptedException throws exception
     */
    public void clickOnListFilter() throws InterruptedException {
        selenium.clickOn(listFilter);
    }

    /**
     * Filter Verify based  on the item list color and filter color
     */
    public void verifyFilter() {
        List<WebElement> itemColors = driver.findElements(By.xpath("//*[@id=\"option-label-color-277-item-17460\"]"));
        WebElement itemColor;
        String warna;
        itemColor = itemColors.get(0);
        System.out.println("color in item : " + itemColor.getAttribute("option-label"));
        warna = itemColor.getAttribute("option-label");

        WebElement filterWarnas = driver.findElement(By.xpath("//*[@id=\"narrow-by-list\"]/div[4]/div[2]/form/div/div[1]/a/div"));
        String filterWarna = filterWarnas.getAttribute("option-label");
        System.out.println("color in filter : " + filterWarnas.getAttribute("option-label"));
        if (warna.equals(filterWarna))
            System.out.println("warna sama");
        else
            System.out.println("warna tidak sama");
    }
}



