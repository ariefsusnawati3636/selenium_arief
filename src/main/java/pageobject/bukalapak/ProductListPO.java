package pageobject.bukalapak;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class ProductListPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    String productName;

    public ProductListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@id='product-explorer-container']/div/div/div[2]/div/div[2]/div[3]/div[1]/div/div/div[3]/div/button")
    private WebElement addToCartButton;

    @FindBy(xpath = "//*[@id='product-explorer-container']/div/div/div[2]/div/div[2]/div[3]/div[1]/div")
    private WebElement productCard;

    @FindBy(xpath = "//div[@class='bl-flex-item mb-8'][1]//div[@class='bl-product-card__description-name']//a")
    private WebElement product;

    @FindBy(xpath = "//div[@class='ml-4 bl-emblem bl-emblem--bukamall_icon te-bukamall_icon']")
    private WebElement iconBukamall;

    /**
     * Hover product
     */
    public void hoverProduct() {
        selenium.hoverElement(productCard);
    }

    /**
     * Click on the first item
     */
    public void clickOnFirstItem() {
        setProductName();
        WebElement productLink = driver.findElement(By.xpath("//div[@class='bl-flex-item mb-8'][1]/*//p/a"));
        productLink.click();
    }

    /**
     * Click add to cart button
     *
     * @throws InterruptedException throws exception
     */
    public void clickAddToCart() throws InterruptedException {
        selenium.clickOn(addToCartButton);
        setProductName();
    }

    /**
     * Set product name
     */
    public void setProductName() {
        this.productName = product.getText();
    }

    /**
     * Get product name
     */
    public String getProductName() {
        return this.productName;
    }

    /**
     * Verify icon Bukamall is displayed
     */
    public boolean verifyIconBukamall() {
        return selenium.isElementAppeared(iconBukamall);
    }
}
