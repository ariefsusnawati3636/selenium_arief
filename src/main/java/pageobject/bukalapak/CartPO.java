package pageobject.bukalapak;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

import java.util.Locale;

import static org.apache.http.util.TextUtils.isEmpty;

public class CartPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    String productName;

    public CartPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//p[@class='bl-text bl-text--subheading-2 bl-text--ellipsis__2']/span")
    private WebElement productOnCart;

    /**
     * Set product name
     */
    public void setProductName() {
        this.productName = productOnCart.getText();
    }

    /**
     * Get product name
     */
    public String getProductName() {
        return this.productName;
    }

    /**
     * Verify product name on cart
     */
    public boolean verifyProductNameOnCart() {
        if (!isEmpty(this.productName)) {
            System.out.println(this.productName.toLowerCase(Locale.ROOT));
            System.out.println(productOnCart.getText().toLowerCase(Locale.ROOT));

            return this.productName.toLowerCase(Locale.ROOT).equals(productOnCart.getText().toLowerCase(Locale.ROOT));
        }
        return false;
    }
}
