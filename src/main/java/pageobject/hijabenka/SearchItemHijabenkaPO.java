package pageobject.hijabenka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class SearchItemHijabenkaPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchItemHijabenkaPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@class='search-trigger']")
    private WebElement btn_search;

    @FindBy(xpath = "//*[@id=\"keyword\"]")
    private WebElement txt_search;

    /**
     * Click search button
     * @throws InterruptedException throws exception
     */
    public void clickOnSearch()throws InterruptedException{
        selenium.click(btn_search);
    }

    /**
     * Enter search item
     * @param searchItem or input search item
     */
    public void inputSearchItem(String searchItem){
        selenium.enterText(txt_search, searchItem, true);
    }

    /**
     * Enter function
     */
    public void enterSearchItem(){
        selenium.hitEnterKey(txt_search, false);
    }

}
