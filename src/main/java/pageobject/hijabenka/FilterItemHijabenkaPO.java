package pageobject.hijabenka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class FilterItemHijabenkaPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public FilterItemHijabenkaPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@id=\"ul-category\"]/li[2]")
    private WebElement btn_filter;

    /**
     * Click filter item
     * @throws InterruptedException throws exception
     */
    public void inputFilter()throws InterruptedException{
        selenium.click(btn_filter);
    }

}
