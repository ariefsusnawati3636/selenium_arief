package pageobject.hijabenka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

import javax.xml.xpath.XPath;

public class SortingItemhijabenkaPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SortingItemhijabenkaPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@id='sort-by']")
    private WebElement btn_sort;

    @FindBy(xpath = "//*[@id='sort-by']/option[4]")
    private WebElement select_low_price;

    /**
     * Click sort button
     * @throws InterruptedException throws exception
     */
    public void clickOnSorting()throws InterruptedException{
        selenium.click(btn_sort);
    }

    /**
     * select sort low price
     * @throws InterruptedException throws exception
     */
    public void selectLowPrice()throws InterruptedException{
        selenium.click(select_low_price);
    }


}
