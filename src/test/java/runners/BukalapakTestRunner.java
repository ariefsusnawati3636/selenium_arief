package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/bukalapak/cucumber-report.json",  "html:target/results/bukalapak"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@bukalapak"}

)
public class BukalapakTestRunner extends BaseTestRunner
{

}
