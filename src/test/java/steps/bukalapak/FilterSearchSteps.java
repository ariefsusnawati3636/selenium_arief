package steps.bukalapak;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageobject.bukalapak.FilterSearchPO;
import pageobject.bukalapak.ProductListPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class FilterSearchSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ProductListPO productlist = new ProductListPO(driver);
    private FilterSearchPO filtersearch = new FilterSearchPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user click on bukamall filter")
    public void user_click_bukamall_filter() throws InterruptedException {
        filtersearch.clickOnCheckboxBukamall();
    }

    @When("user hover filter scrollbar")
    public void user_hover_filter_scrollbar() {
        filtersearch.hoverFilterScrollbar();
    }

    @When("user scroll to bukamall checkbox")
    public void user_scroll_bukamall_checkbox() throws InterruptedException {
        filtersearch.scrollToCheckboxBukamall();
    }

    @Then("verify bukamall icon")
    public void verify_bukamall_icon() {
        Assert.assertTrue(productlist.verifyIconBukamall());
    }
}
