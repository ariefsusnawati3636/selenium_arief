package steps.bukalapak;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import pageobject.bukalapak.CheckoutPO;
import pageobject.bukalapak.ProductListPO;
import pageobject.bukalapak.ProductPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.Locale;

public class CheckoutSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ProductListPO productlist = new ProductListPO(driver);
    private ProductPO product = new ProductPO(driver);
    private CheckoutPO checkout = new CheckoutPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user click on item")
    public void user_click_first_item() {
        productlist.clickOnFirstItem();
    }

    @When("user click on buy now button")
    public void user_click_buy_now() throws InterruptedException {
        product.clickOnBuyNowButton();
    }

    @And("verify product name on checkout")
    public void verify_item_name_on_checkout() {
        checkout.setProductName();
        String productnameonlist = productlist.getProductName().toLowerCase(Locale.ROOT);
        String productnameoncheckout = checkout.getProductName().toLowerCase(Locale.ROOT);
        Assert.assertEquals(productnameoncheckout, productnameonlist);
    }

}
