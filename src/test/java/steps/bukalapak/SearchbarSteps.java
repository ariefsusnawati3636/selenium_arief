package steps.bukalapak;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobject.bukalapak.SearchbarPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;
import java.util.Locale;


public class SearchbarSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SearchbarPO searchbar = new SearchbarPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user input search with keyword {string}")
    public void user_input_search_keyword(String keyword) throws InterruptedException {
        searchbar.enterSearchKeyword(keyword);
    }

    @When("user input search with keyword {string} and click on search submit button")
    public void user_input_search_keyword_and_click(String keyword) throws InterruptedException {
        searchbar.enterSearchKeyword(keyword);
        searchbar.clickOnSubmitSearch();
    }

    @Then("verify search result with keyword {string}")
    public void verify_search_result(String keyword) {
        List<WebElement> productnames = driver.findElements(By.xpath("//div[@class='bl-flex-container flex-wrap is-gutter-16']/div//div[@class='bl-product-card__description-name']//a"));
        WebElement productName;
        String lowercaseName;
        for (int i = 0; i < 9; i++) {
            productName = productnames.get(i);
            System.out.println(productName.getText());
            lowercaseName = productName.getText().toLowerCase(Locale.ROOT);
            keyword = keyword.toLowerCase(Locale.ROOT);
            Assert.assertTrue(lowercaseName.contains(keyword));
        }
    }
}