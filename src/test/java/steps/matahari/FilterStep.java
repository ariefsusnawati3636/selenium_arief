package steps.matahari;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobject.matahari.FilterBarangPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class FilterStep {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private FilterBarangPO filter = new FilterBarangPO(driver);

    @When("user click filter color")
    public void user_click_filter() throws InterruptedException {
        filter.clickOnSwitchFilter();
    }

    @When("user chose colors")
    public void user_chose_category_from_filter() throws InterruptedException {
        filter.clickOnListFilter();

    }

    @Then("verify item")
    public void verify_item() {
       filter.verifyFilter();
    }
}
