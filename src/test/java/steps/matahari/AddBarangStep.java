package steps.matahari;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobject.matahari.AddToCartPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;


public class AddBarangStep {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private AddToCartPO Add = new AddToCartPO(driver);

    @Given("user click button beli")
    public void user_click_button_beli() throws InterruptedException {
      Add.clickOnBuyButton();
    }

    @When("user chose size")
    public void user_chose_size()throws InterruptedException  {
        Add.clickOnChoseSize();
    }

    @When("user chose color")
    public void user_chose_color() throws InterruptedException {
        Add.clickOnChoseColor();
    }
    @When("user klik add to cart")
    public void user_klik_add_to_cart() throws InterruptedException {
     Add.clickOnAddToCart();
    }

    @When("user click keranjang")
    public void user_click_keranjang() throws InterruptedException {
       Add.clickOnShowCart();
    }

    @Then("verify item name on keranjang")
    public boolean verify_item_name_on_keranjang() throws InterruptedException  {
        if (Add.verifyItemNameCart()){
            return true;
        }
        return  false;
    }



}