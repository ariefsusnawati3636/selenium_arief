package steps.sociolla;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobject.sociolla.AddToCartPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class AddToCartSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private AddToCartPO AddToCart = new AddToCartPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user click shop now button")
    public void user_click_shop_now_button() throws InterruptedException{
        AddToCart.clickShopNowButton();
    }
    @When("user click add to bag button")
    public void user_click_add_to_bag_button() throws InterruptedException{
        AddToCart.clickOnAddToBagButton();
    }

    @When("user click my bag")
    public void user_click_my_bag() throws InterruptedException{
        AddToCart.clickMyBagText();
    }
    @When("user click checkout button")
    public void user_click_checkout_button() throws InterruptedException{
        AddToCart.clickOnCheckoutButton();
    }
    @Then("verify buy now sociolla")
    public boolean verify_buy_now_sociolla() {
        if (AddToCart.verifyBuyNow()) {
            return true;
        }
        return false;
    }
}
