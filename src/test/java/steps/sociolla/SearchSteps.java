package steps.sociolla;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageobject.sociolla.SearchPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class SearchSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SearchPO Search = new SearchPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user input barang {string} and enter")
    public void user_input_barang(String barang)throws InterruptedException{
        Search.inputSearch(barang);
        Search.hardWait(20);
        Search.enterSearch();
        Search.hardWait(20);
    }
    @Then("verify product name by keyword {string}")
    public void verify_product_name_by_keyword(String keyword) {
        for(int i= 1; i<11; i++) {
            String element = driver.findElement(By.xpath("(//*[@class='product__name'])["+i+"]")).getText();
            System.out.println(element);
        }
    }
}
