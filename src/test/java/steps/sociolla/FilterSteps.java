package steps.sociolla;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobject.sociolla.LoginPO;
import pageobject.sociolla.filterPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class FilterSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private filterPO Filter = new filterPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user click filter terlaris sociolla")
    public void user_click_filter_terlaris_button() throws InterruptedException {
        Filter.clickOnTerlarisButton();
    }
    @When("user click filter review sociolla")
    public void user_click_filter_review_button() throws InterruptedException {
        Filter.clickOnReviewButton();
    }
    @When("user click filter sale sociolla")
    public void user_click_filter_sale_button() throws InterruptedException {
        Filter.clickOnSaleButton();
    }
    @When("user click filter harga sociolla")
    public void user_click_filter_harga_button() throws InterruptedException {
        Filter.clickOnHargaButton();
    }
    @When("user click filter terbaru sociolla")
    public void user_click_filter_terbaru_button() throws InterruptedException {
        Filter.clickOnTerbaruButton();
    }
    @When("user click filter abjad sociolla")
    public void user_click_filter_abjad_button() throws InterruptedException {
        Filter.clickOnAbjadButton();
    }
    @Then("verify filter sociolla")
    public boolean verify_filter_sociolla() {
        if (Filter.verifyFilter()) {
            return true;
        }
        return false;
    }
}
