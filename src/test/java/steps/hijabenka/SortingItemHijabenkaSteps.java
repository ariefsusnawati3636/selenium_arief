package steps.hijabenka;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageobject.hijabenka.SortingItemhijabenkaPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;
import java.util.List;

public class SortingItemHijabenkaSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SortingItemhijabenkaPO sortingItemhijabenkaPO = new SortingItemhijabenkaPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user click on sorting button")
    public void user_click_on_sorting() throws InterruptedException{
        sortingItemhijabenkaPO.clickOnSorting();
        sortingItemhijabenkaPO.selectLowPrice();

    }

    @Then("verify the sorting")
    public void verify_the_sorting() {
        List<WebElement> list_of_products_price = driver.findElements(By.xpath("//*[@class='detail-right']/p[2]"));
        String low_price;
        String product_price;

        for(int i=0;i<10;i++) {
            product_price = list_of_products_price.get(i).getText();
            product_price = product_price.replaceAll("[^0-9]", "");
            System.out.println(product_price);
            low_price = list_of_products_price.get(i).getText();
            low_price = low_price.replaceAll("[^0-9]", "");
            System.out.println(low_price);
            Assert.assertTrue(product_price.contains(low_price));
        }
    }
}
