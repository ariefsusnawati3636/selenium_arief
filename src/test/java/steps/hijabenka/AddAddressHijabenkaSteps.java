package steps.hijabenka;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import pageobject.hijabenka.AddAddressHijabenkaPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class AddAddressHijabenkaSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddAddressHijabenkaPO addAddressHijabenkaPO = new AddAddressHijabenkaPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user click on account button")
    public void user_click_on_account_button()throws InterruptedException {
        addAddressHijabenkaPO.clickOnAccount();
    }

    @And("user click on account setting button and click on add new address")
    public void user_click_on_account_setting_button_and_click_on_add_new_address() throws InterruptedException {
        addAddressHijabenkaPO.clickOnSetting();
        addAddressHijabenkaPO.clickOnAddAddress();
    }

    @And("user fill the field {string} {string} {string}")
    public void user_fill_the_field(String address, String postalCode, String phone) throws InterruptedException{
        addAddressHijabenkaPO.enterAddress(address);
        addAddressHijabenkaPO.selectProvince();
        addAddressHijabenkaPO.selectCity();
        addAddressHijabenkaPO.enterPostalCode(postalCode);
        addAddressHijabenkaPO.enterPhone(phone);
        addAddressHijabenkaPO.clickOnSaveAddress();
    }

    @Then("verify the address")
    public void verify_the_address() {
        addAddressHijabenkaPO.verifyAddress();
    }


}
