package steps.hijabenka;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import pageobject.hijabenka.AddItemHijabenkaPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class AddItemHijabenkaSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddItemHijabenkaPO addItemhijabenkaPO = new AddItemHijabenkaPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("Add the barang and checkout")
    public void Add_the_barang_and_checkout() throws InterruptedException{
        addItemhijabenkaPO.clickOnAddItem();
        addItemhijabenkaPO.clickOnSize();
        addItemhijabenkaPO.clickOnOrderNow();
    }

    @Then("verify the item Checkout")
    public void verify_the_item_Checkout()throws InterruptedException{
        addItemhijabenkaPO.verifyItem();
    }
}
